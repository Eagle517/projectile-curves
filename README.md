# Projectile Curves
A DLL for BCS that allows projectiles to curve with included client side simulation.

## Usage
Inject the DLL into Blockland via [RedBlocklandLoader](https://gitlab.com/Eagle517/redblocklandloader) or your favorite DLL injector.
