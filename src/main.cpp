#include <stdio.h>
#include <map>
#include <math.h>
#include <windows.h>

#include "scripts.hpp"
#include "TSFuncs.hpp"

struct Vector3f
{
	float x;
	float y;
	float z;

	Vector3f add(Vector3f b)
	{
		return (Vector3f){x + b.x, y + b.y, z + b.z};
	}

	Vector3f sub(Vector3f b)
	{
		return (Vector3f){x - b.x, y - b.y, z - b.z};
	}

	Vector3f scale(float scalar)
	{
		return (Vector3f){x*scalar, y*scalar, z*scalar};
	}

	Vector3f cross(Vector3f b)
	{
		return (Vector3f){
			y*b.z - z*b.y,
			z*b.x - x*b.z,
			x*b.y - y*b.x
		};
	}

	float lenSqr()
	{
		return x*x + y*y + z*z;
	}

	float len()
	{
		return sqrtf(x*x + y*y + z*z);
	}

	Vector3f normalize()
	{
		float len = this->len();
		if(len > 0.f)
			return (Vector3f){x/len, y/len, z/len};
		else
			return (Vector3f){0.f, 0.f, 0.f};
	}

	void print()
	{
		BlPrintf("  %g %g %g", x, y, z);
	}
};

struct Curve
{
	Vector3f start;
	Vector3f end;
	Vector3f mid;
	float speed;
	float currentStep;

	Curve() : currentStep(0.05f) {}

	Vector3f getPointOnCurve(float s)
	{
		float a = 1-s;
		return start.scale(a*a).add(mid.scale(2*a*s)).add(end.scale(s*s));
	}

	Vector3f step()
	{
		if(currentStep > 1.f)
			currentStep = 1.f;

		Vector3f point = getPointOnCurve(currentStep);
		currentStep += 0.001024f*(speed);
		return point;
	}

	void print()
	{
		BlPrintf("CURVE:");
		start.print();
		end.print();
		mid.print();
		BlPrintf("  %g\n", speed);
	}
};

std::map<unsigned int, Curve *> gServerCurveMap;
std::map<unsigned int, ADDR> gCurveClients;

std::map<unsigned int, Curve *> gClientCurveMap;
bool gClientCurvesEnabled;

bool gGameConnection__onConnectionEstablishedHookCalled;

BlFunctionDef(void, __thiscall, NetObject__setMaskBits, ADDR, unsigned int);

/* Checks the object's Net Flags to see if it's a ghost.
   If it is isn't, that means its a server object. */
bool isServerObject(ADDR obj)
{
	return (*(ADDR *)(obj + 68) & 2) == 0;
}

/* On the first ::unpackUpdate call, an object
   has no SimObject ID set. So the solution is
   to rely on its Ghost ID for the client side
   of things, and rely on its SimObject ID for
   the server side of things. */
unsigned int getProjectileMapID(ADDR obj)
{
	if(isServerObject(obj))
		return *(unsigned int *)(obj + 32); //SimObject ID
	else
		return *(unsigned int *)(obj + 72); //Ghost ID
}

bool hasProjectileCurve(ADDR obj)
{
	unsigned int id = getProjectileMapID(obj);

	if(isServerObject(obj))
		return gServerCurveMap.count(id) > 0;
	else
		return gClientCurveMap.count(id) > 0;
}

Curve *getProjectileCurve(ADDR obj)
{
	if(hasProjectileCurve(obj))
	{
		unsigned int id = getProjectileMapID(obj);
		if(isServerObject(obj))
			return gServerCurveMap[id];
		else
			return gClientCurveMap[id];
	}
	else
		return NULL;
}

void eraseProjectileCurve(ADDR obj)
{
	if(hasProjectileCurve(obj))
	{
		unsigned int id = getProjectileMapID(obj);
		Curve *curve = getProjectileCurve(obj);

		if(isServerObject(obj))
			gServerCurveMap.erase(id);
		else
			gClientCurveMap.erase(id);

		delete curve;
	}
}

void insertProjectileCurve(ADDR obj, Curve *curve)
{
	eraseProjectileCurve(obj);

	unsigned int id = getProjectileMapID(obj);

	if(isServerObject(obj))
		gServerCurveMap.insert(std::make_pair(id, curve));
	else
		gClientCurveMap.insert(std::make_pair(id, curve));
}

BlFunctionDef(unsigned int, __thiscall, Projectile__packUpdate, ADDR, ADDR, unsigned int, ADDR);
unsigned int __fastcall Projectile__packUpdateHook(ADDR, void *, ADDR, unsigned int, ADDR);
BlFunctionHookDef(Projectile__packUpdate);

/* This is where the server writes all the relevant data
   about a projectile to a client. I hook it to append
   data about the curve so the client can properly
   simulate it on their end. */
unsigned int __fastcall Projectile__packUpdateHook(ADDR obj, void *blank, ADDR con, unsigned int mask, ADDR stream)
{
#define WRITEFLOAT(data) (*(void (__thiscall **)(ADDR, signed int, void *))(*(ADDR *)stream + 12))(stream, 4, &data)
#define WRITEFLAG(bitstream, flag) (bool)(*(int (__thiscall **)(ADDR, bool))(*(ADDR *)bitstream + 48))(bitstream, flag)

	ADDR gamecon = con + 160;
	unsigned int gameconID = *(unsigned int *)(gamecon + 32);
	bool clientHasCurves = gCurveClients.count(gameconID) > 0;

	//If a client has curves, we want them to use our flag instead of the bounce flag and vice versa
	if((mask & (16 | 128)) != 0 && (mask & 2) == 0)
	{
		if(clientHasCurves)
			mask &= ~16;
		else
			mask &= ~128;
	}

	Projectile__packUpdateHookOff();
	unsigned int ret = Projectile__packUpdate(obj, con, mask, stream);
	Projectile__packUpdateHookOn();

	//Only write the extra network data to clients that have the DLL
	if(clientHasCurves)
	{
		Curve *curve = getProjectileCurve(obj);

		if(WRITEFLAG(stream, (mask & 64) == 64 && curve != NULL))
		{
			//curve->print();

			WRITEFLOAT(curve->start.x); WRITEFLOAT(curve->start.y); WRITEFLOAT(curve->start.z);
			WRITEFLOAT(curve->end.x);   WRITEFLOAT(curve->end.y);   WRITEFLOAT(curve->end.z);
			WRITEFLOAT(curve->mid.x);   WRITEFLOAT(curve->mid.y);   WRITEFLOAT(curve->mid.z);
			WRITEFLOAT(curve->speed);
		}

		/* This is intended for keeping the client up to speed with the server.
		   However, I found it causes more issues than it's worth. */
		//if(WRITEFLAG(stream, (mask & 128) == 128 && curve != NULL))
		//	WRITEFLOAT(curve->currentStep);
	}

	return ret;

#undef WRITEFLAG
#undef WRITEFLOAT
}

BlFunctionDef(void, __thiscall, Projectile__unpackUpdate, ADDR, ADDR, ADDR);
void __fastcall Projectile__unpackUpdateHook(ADDR, void *, ADDR, ADDR);
BlFunctionHookDef(Projectile__unpackUpdate);

/* And this is where the client reads what the server sent. */
void __fastcall Projectile__unpackUpdateHook(ADDR obj, void *blank, ADDR con, ADDR stream)
{
#define READFLOAT(data) (*(void (__thiscall **)(ADDR, signed int, void *))(*(ADDR *)stream + 8))(stream, 4, &data)
#define READFLAG(bitstream) (*(int (__thiscall **)(ADDR))(*(ADDR *)stream + 52))(bitstream)

	Projectile__unpackUpdateHookOff();
	Projectile__unpackUpdate(obj, con, stream);
	Projectile__unpackUpdateHookOn();

	/* Flags are read under the assumption you know when to read them.
	   TGE expects perfect reflection of packet writing and reading. */
	if(READFLAG(stream))
	{
		Curve *curve = new Curve();

		READFLOAT(curve->start.x); READFLOAT(curve->start.y); READFLOAT(curve->start.z);
		READFLOAT(curve->end.x);   READFLOAT(curve->end.y);   READFLOAT(curve->end.z);
		READFLOAT(curve->mid.x);   READFLOAT(curve->mid.y);   READFLOAT(curve->mid.z);
		READFLOAT(curve->speed);

		//curve->print();

		*(Vector3f *)(obj + 552) = curve->start;

		insertProjectileCurve(obj, curve);
	}

	//if(READFLAG(stream))
	//{
	//	float step;
	//	READFLOAT(step);
	//	//BlPrintf("Client received step: %g", step);

	//	Curve *curve = getProjectileCurve(obj);
	//	if(curve != NULL)
	//	{
	//		if(step > curve->currentStep)
	//		{
	//			//BlPrintf("Step newer; updating");
	//			curve->currentStep = step;
	//		}
	//	}
	//}

#undef READFLOAT
}

BlFunctionDef(void, __thiscall, Projectile__processTick, ADDR, ADDR);
void __fastcall Projectile__processTickHook(ADDR, void *, ADDR);
BlFunctionHookDef(Projectile__processTick);

/* processTick handles all the physics of the projectile.
   As such, I hook it to add in my own. */
void __fastcall Projectile__processTickHook(ADDR obj, void *blank, ADDR move)
{
	ADDR dataBlock = *(ADDR *)(obj + 516);
	float gravityMod = *(float *)(dataBlock + 156);

	unsigned int lifetime = *(unsigned int *)(dataBlock + 160);
	unsigned int mCurrTick = *(unsigned int *)(obj + 600);

	Vector3f newVel;
	Curve *curve = getProjectileCurve(obj);

	if(curve != NULL)
	{
		if(mCurrTick >= lifetime)
		{
			eraseProjectileCurve(obj);
			curve = NULL;
		}
		else
		{
			Vector3f currPos = *(Vector3f *)(obj + 552);
			Vector3f nextPos = curve->step();

			newVel = nextPos.sub(currPos).normalize().scale(curve->speed);
			newVel.z -= 24.9f * curve->currentStep;

			*(Vector3f *)(obj + 564) = newVel;
			*(float *)(dataBlock + 156) = 0.f; //Disable gravity
		}
	}

	Projectile__processTickHookOff();
	Projectile__processTick(obj, move);
	Projectile__processTickHookOn();

	if(curve != NULL)
	{
		/* ::processTick will set the bounce or explosion mask (16 | 32) on the server
		   which I use to know when to cut the curve short (i.e. the projectile hit a wall).
		   However, it's harder to determine it for the client. I'm relying on the idea that
		   the cross product of the old vel and the new vel will never be that high during a
		   curve. */
		if(curve->currentStep >= 1 || *(unsigned int *)(obj + 52) & (16 | 32) || newVel.cross(*(Vector3f *)(obj + 564)).lenSqr() > 0.001)
		{
			eraseProjectileCurve(obj);

			if(isServerObject(obj))
				NetObject__setMaskBits(obj, 16);
		}
		else if(isServerObject(obj))
			NetObject__setMaskBits(obj, 128 | 16);

		*(float *)(dataBlock + 156) = gravityMod; //Restore gravity
	}
}

BlFunctionDef(void, __thiscall, Projectile__onRemove, ADDR);
void __fastcall Projectile__onRemoveHook(ADDR);
BlFunctionHookDef(Projectile__onRemove);

/* This ensures all curves get cleaned up. */
void __fastcall Projectile__onRemoveHook(ADDR obj)
{
	eraseProjectileCurve(obj);

	Projectile__onRemoveHookOff();
	Projectile__onRemove(obj);
	Projectile__onRemoveHookOn();
}

BlFunctionDef(int, __fastcall, GameConnection__onConnectionEstablished, int, void *, char);
int __fastcall GameConnection__onConnectionEstablishedHook(int, void *, char);
BlFunctionHookDef(GameConnection__onConnectionEstablished);

/* Badspot disables all active packages when a server goes to start.
   As a result, I cannot load the server package with the client package.
   So, I hook this method and load the package here. */
int __fastcall GameConnection__onConnectionEstablishedHook(int a1, void *edx, char a7)
{
	tsf_Evalf("%s", SERVER_SCRIPT);
	gGameConnection__onConnectionEstablishedHookCalled = true;
	GameConnection__onConnectionEstablishedHookOff();

	return GameConnection__onConnectionEstablished(a1, edx, a7);
}

void TS_Projectile__setPosition(ADDR obj, int argc, const char *argv[])
{
	if(isServerObject(obj))
	{
		float x = 0.f;
		float y = 0.f;
		float z = 0.f;

		sscanf(argv[2], "%f %f %f", &x, &y, &z);
		*(float *)(obj + 552) = x;
		*(float *)(obj + 556) = y;
		*(float *)(obj + 560) = z;

		NetObject__setMaskBits(obj, 16);
	}
}

void TS_Projectile__setVelocity(ADDR obj, int argc, const char *argv[])
{
	if(isServerObject(obj))
	{
		float x = 0.f;
		float y = 0.f;
		float z = 0.f;

		sscanf(argv[2], "%f %f %f", &x, &y, &z);
		*(float *)(obj + 564) = x;
		*(float *)(obj + 568) = y;
		*(float *)(obj + 572) = z;

		NetObject__setMaskBits(obj, 16);
	}
}

void TS_Projectile__curve(ADDR obj, int argc, const char *argv[])
{
	if(isServerObject(obj))
	{
		Curve *curve = new Curve();
		sscanf(argv[2], "%f %f %f", &curve->start.x, &curve->start.y, &curve->start.z);
		sscanf(argv[3], "%f %f %f", &curve->end.x,   &curve->end.y,   &curve->end.z);
		sscanf(argv[4], "%f %f %f", &curve->mid.x,   &curve->mid.y,   &curve->mid.z);
		sscanf(argv[5], "%f", &curve->speed);

		*(Vector3f *)(obj + 552) = curve->start;

		insertProjectileCurve(obj, curve);
		NetObject__setMaskBits(obj, 64);
	}
}

void TS_GameConnection__setProjectileCurveEnabled(ADDR obj, int argc, const char *argv[])
{
	unsigned int id = *(unsigned int *)(obj + 32);
	ADDR netconn = obj - 160;
	bool enabled = gCurveClients.count(netconn) > 0;

	if(atoi(argv[2]) || stricmp(argv[2], "true") == 0)
	{
		if(!enabled)
			gCurveClients.insert(std::make_pair(id, netconn));
	}
	else
	{
		if(enabled)
			gCurveClients.erase(id);
	}
}

void TS_setProjectileCurvesEnabled(ADDR obj, int argc, const char *argv[])
{
	bool lastEnabled = gClientCurvesEnabled;
	gClientCurvesEnabled = atoi(argv[1]) || stricmp(argv[1], "true") == 0;

	if(lastEnabled != gClientCurvesEnabled)
	{
		if(gClientCurvesEnabled)
			Projectile__unpackUpdateHookOn();
		else
			Projectile__unpackUpdateHookOff();
	}
}

bool init()
{
	BlInit;

	if(!tsf_InitInternal())
		return false;

	gClientCurvesEnabled = false;
	gGameConnection__onConnectionEstablishedHookCalled = false;

	BlScanFunctionHex(NetObject__setMaskBits,   "8B 41 34 85 C0 75 19 8B 15 ? ? ? ? 85 D2 74 09 89 51 3C 89 4A 38 8B 41 34 89 0D ? ? ? ? 0B 44 24 04");
	BlScanFunctionHex(Projectile__packUpdate,   "83 EC 14 53 55 56 8B 74 24 28 8B D9");
	BlScanFunctionHex(Projectile__unpackUpdate, "55 8B EC 83 E4 F8 81 EC ? ? ? ? A1 ? ? ? ? 33 C4 89 84 24 ? ? ? ? 8B 45 08 53 56 57 8B 7D 0C");
	BlScanFunctionHex(Projectile__processTick,  "55 8B EC 83 E4 F8 81 EC ? ? ? ? A1 ? ? ? ? 33 C4 89 84 24 ? ? ? ? 56 57 8B F9 89 7C 24 5C");
	BlScanFunctionHex(Projectile__onRemove,     "53 8B D9 8B 8B ? ? ? ? 85 C9 74 28");
	BlScanFunctionHex(GameConnection__onConnectionEstablished, "83 EC 4C A1 ? ? ? ? 33 C4 89 44 24 48 80 7C 24 ? ?");

	tsf_AddConsoleFunc(NULL, "Projectile", "setPosition", TS_Projectile__setPosition, "(Vector3F pos)", 3, 3);
	tsf_AddConsoleFunc(NULL, "Projectile", "setVelocity", TS_Projectile__setVelocity, "(Vector3F vel)", 3, 3);
	tsf_AddConsoleFunc(NULL, "Projectile", "curve",       TS_Projectile__curve,       "(Vector3f start, Vector3f end, Vector3f mid, float speed)", 6, 6);

	tsf_AddConsoleFunc(NULL, "GameConnection", "setProjectileCurvesEnabled", TS_GameConnection__setProjectileCurveEnabled, "(bool enabled)", 3, 3);
	tsf_AddConsoleFunc(NULL, NULL,             "setProjectileCurvesEnabled", TS_setProjectileCurvesEnabled,                "(bool enabled)", 2, 2);

	Projectile__packUpdateHookOn();
	Projectile__processTickHookOn();
	Projectile__onRemoveHookOn();
	GameConnection__onConnectionEstablishedHookOn();

	tsf_Evalf("%s", CLIENT_SCRIPT);

	BlPrintf("%s: init'd", PROJECT_NAME);
	return true;
}

bool deinit()
{
	if(gClientCurvesEnabled)
		Projectile__unpackUpdateHookOff();

	if(!gGameConnection__onConnectionEstablishedHookCalled)
		GameConnection__onConnectionEstablishedHookOff();

	Projectile__packUpdateHookOff();
	Projectile__processTickHookOff();
	Projectile__onRemoveHookOff();

	BlPrintf("%s: deinit'd", PROJECT_NAME);
	return true;
}

bool __stdcall DllMain(HINSTANCE hinstance, unsigned int reason, void *reserved)
{
	switch(reason)
	{
		case DLL_PROCESS_ATTACH:
			return init();
		case DLL_PROCESS_DETACH:
			return deinit();
		default:
			return true;
	}
}

extern "C" void __declspec(dllexport) __cdecl PROJECT_EXPORT(){}
