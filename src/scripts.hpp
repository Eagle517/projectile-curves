//Sometimes TorqueScript is just better.

const char *CLIENT_SCRIPT =
"package Client_ProjectileCurves {"
	"function clientCmdProjectileCurvesHandshake(%enabled) {"
		"commandToServer('ProjectileCurvesHandshake');"
		"setProjectileCurvesEnabled(%enabled);"
	"}"

	"function disconnect(%doReconnect) {"
		"setProjectileCurvesEnabled(false);"
		"parent::disconnect(%doReconnect);"
	"}"
"};"
"activatePackage(\"Client_ProjectileCurves\");";

const char *SERVER_SCRIPT =
"package Server_ProjectileCurves {"
	"function serverCmdProjectileCurvesHandshake(%client) {"
		"echo(\"ProjectileCurves: server handshake\");"
		"%client.setProjectileCurvesEnabled(true);"
	"}"

	"function GameConnection::autoAdminCheck(%this) {"
		"echo(\"ProjectileCurves: autoAdminChcek\");"
		"%ret = parent::autoAdminCheck(%this);"
		"commandToClient(%this, 'ProjectileCurvesHandshake', true);"
		"return %ret;"
	"}"

	"function GameConnection::onClientLeaveGame(%this) {"
		"echo(\"ProjectileCurves: onClientLeaveGame\");"
		"%this.setProjectileCurvesEnabled(false);"
		"parent::onClientLeaveGame(%this);"
	"}"
"};"
"activatePackage(\"Server_ProjectileCurves\");";
