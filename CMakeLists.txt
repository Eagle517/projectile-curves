cmake_minimum_required(VERSION 3.0)
project(ProjectileCurves)

set(CMAKE_CXX_FLAGS "-static -static-libgcc -static-libstdc++ -Wall -Werror -O2")
add_compile_definitions(PROJECT_NAME="${PROJECT_NAME}")
add_compile_definitions(PROJECT_EXPORT=${PROJECT_NAME})
add_compile_definitions(RBH_DEBUGPRINT=false)

add_library(${PROJECT_NAME} SHARED
	src/main.cpp
	src/TSFuncs.cpp
	src/RedoBlHooks.cpp
)

set_target_properties(${PROJECT_NAME} PROPERTIES PREFIX "")
target_link_libraries(
	${PROJECT_NAME}
	psapi
)
